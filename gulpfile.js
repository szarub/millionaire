var gulp = require('gulp');
var sass = require('gulp-sass');
var bom = require('gulp-bom');
var crLfReplace = require('gulp-cr-lf-replace');

var dist = 'bin';

gulp.task('scss', function () {
    gulp.src('src/styles.scss')
    .pipe(sass(
        { includePaths : ['src/'] }
    ))
    .pipe(crLfReplace(
        { changeCode: 'CR+LF' }
    ))
    .pipe(gulp.dest(dist));
});
gulp.task('images', function () {
    gulp.src('src/img/*')
    .pipe(gulp.dest(dist + '/img'));
});
gulp.task('sounds', function () {
    gulp.src('src/sounds/*.mp3')
    .pipe(gulp.dest(dist + '/sounds'));
});
gulp.task('scripts', function () {
    gulp.src('src/*.js')
    .pipe(bom())
    .pipe(gulp.dest(dist));
});
gulp.task('html', function () {
    gulp.src('src/game.html')
    .pipe(gulp.dest(dist));
});

gulp.task('watch', ['build'], function () {
    gulp.watch('src/styles.scss', ['scss']);
    gulp.watch('src/*.js', ['scripts']);
    gulp.watch('src/game.html', ['html']);
});
gulp.task('build', ['html', 'images', 'sounds', 'scss', 'scripts']);