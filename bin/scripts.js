﻿angular
    .module('millionaire', [])
    .controller('main', function ($scope) {
        $scope.answerChoices = ['A', 'B', 'C', 'D'];
        $scope.prices = ['1 000 000', '500 000', '250 000', '125 000', '64 000',
            '32 000', '16 000', '8 000', '4 000', '2 000',
            '1 000', '500', '300', '200', '100']; 
        $scope.current = 1;
        $scope.questions = questions;
        var currQuestion = $scope.questions[$scope.current - 1];
        $scope.currQuestion = {
            question: currQuestion.question,
            answers: _.shuffle(_.map(currQuestion.answers, function (value, index) {
                return {
                    isCorrect: index === 0,
                    label: value
                }
            }))
        };
    });